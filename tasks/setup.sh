#!/bin/sh
path=`pwd`
escape_path=`pwd | sed "s/\//\\\\\\\\\//g"`

sh $path/tasks/teardown.sh 1

template=$path/templates/jp.co.onopon.fukushi_checker.plist.template
sed -e "s/__PATH__/$escape_path/g" $template > $path/storage/jp.co.onopon.fukushi_checker.plist
ln -s $path/storage/jp.co.onopon.fukushi_checker.plist ~/Library/LaunchAgents/jp.co.onopon.fukushi_checker.plist
launchctl load ~/Library/LaunchAgents/jp.co.onopon.fukushi_checker.plist

kill_template=$path/templates/jp.co.onopon.fukushi_checker_kill.plist.template
sed -e "s/__PATH__/$escape_path/g" $kill_template > $path/storage/jp.co.onopon.fukushi_checker_kill.plist
ln -s $path/storage/jp.co.onopon.fukushi_checker_kill.plist ~/Library/LaunchAgents/jp.co.onopon.fukushi_checker_kill.plist
launchctl load ~/Library/LaunchAgents/jp.co.onopon.fukushi_checker_kill.plist

sleepwatcher_template=$path/templates/jp.co.onopon.fukushi_checker_sleepwatcher.plist.template
sed -e "s/__PATH__/$escape_path/g" $sleepwatcher_template > $path/storage/jp.co.onopon.fukushi_checker_sleepwatcher.plist
ln -s $path/storage/jp.co.onopon.fukushi_checker_sleepwatcher.plist ~/Library/LaunchAgents/jp.co.onopon.fukushi_checker_sleepwatcher.plist
launchctl load ~/Library/LaunchAgents/jp.co.onopon.fukushi_checker_sleepwatcher.plist
echo "設定が完了しました。"
exit 0
