#!/bin/sh
path="../"
if [ "$1" != "" ]
then
  path=$1
fi

current_title=`/usr/bin/php $path/tasks/title_parser.php`
if [ -z "$current_title" ]
then
  exit 0
fi

latest_title=''
if test -e "$path/storage/logs/latest_title.txt"; then
  latest_title=`cat $path/storage/logs/latest_title.txt`
fi
if [[ ${current_title} != ${latest_title} ]]; then
  result=`/usr/bin/osascript $path/tasks/display_dialog.scpt`
  if [[ $result = "今すぐ確認する" ]]; then
    echo $current_title > $path/storage/logs/latest_title.txt
    /usr/bin/osascript $path/tasks/open_url.scpt
  fi 
fi
exit 0
