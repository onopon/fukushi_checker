#!/bin/sh
path=`pwd`
if [ "$1" != "" ]
then
  path=$1
fi

pid=`ps ax | grep $path/tasks/executor.sh | grep -v grep | awk '{print $1}'`
if [ "$pid" != "" ]
then
  kill $pid
fi
