#!/bin/sh
path=`pwd`
sh $path/tasks/kill.sh $path
plists=("jp.co.onopon.fukushi_checker.plist" "jp.co.onopon.fukushi_checker_kill.plist" "jp.co.onopon.fukushi_checker_sleepwatcher.plist")
for plist in ${plists[@]}; do
  launchctl unload ~/Library/LaunchAgents/$plist
  rm ~/Library/LaunchAgents/$plist
  rm $path/storage/$plist
done

if [ "$1" = "" ]
then
  echo "チェックを終了しました。"
fi
exit 0
