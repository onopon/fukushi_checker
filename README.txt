===============================
研修チェッカー
===============================

【概要】
これは、規定のページの研修一覧が更新された場合、1時間に1度PC上にポップアップを表示して通知するMac専用のシステムです。
下記が具体的な動作内容です。
・1時間に1度研修一覧をチェックします（PCを開いている間、毎時0分のタイミングでチェックが走ります）。
・最終更新日が変わっていた場合はポップアップを表示します.
・ポップアップの「今すぐ確認する」ボタンを押下すると研修一覧のホームページが表示されます。
・ポップアップは、表示後5分間何もボタンを押されなければ非表示となります（次回のチェックタイミングで、再び表示します）。


【動作条件】
MacOS10.4以降であれば動くはず・・・


【ファイル構成】
setup.command    : 本システムをセットアップするためのコマンドファイルです。
teardown.command : 本システムを終了するためのコマンドファイルです。
tasks            : 本アプリを稼働させるためのスクリプトファイルが入っています。
storage          : setup.app を稼働させたときに生成されるファイルや、システムエラーのログ置き場です。
templates        : テンプレートファイル置き場です。setup.app実行時に利用します。
sleepwatcher_2.2 : Macのスリープ/復帰を検知するシステムです。（ http://www.bernhard-baehr.de/ ）
aa               : アスキーアート置き場です。
README.txt       : このファイルです。


【使用方法】
●セットアップの方法
1. 本フォルダ（fukushi_checker）を今後移動等の変更をしない場所においてください。
2. setup.command をダブルクリックしてください。
※ 初回のみsetup.command を右クリック（shift + control + クリック）で「開く」を選択し、開いてください。
3. ウィンドウの上部で猫が喜んでいれば設定完了です。
4. 出てきたウィンドウを閉じてください。
以上でセットアップは完了です。

●本システムの終了方法
1. teardown.command をダブルクリックしてください。
※ 初回のみteardown.command を右クリック（shift + control + クリック）で「開く」を選択し、開いてください。
2. ウィンドウの上部に猫が悲しんでいれば解除完了です。
3. 出てきたウィンドウを閉じてください。
以上で本システムを終了させることができます。


【注意事項】
・本システムのセットアップを行った後に、本フォルダの移動は行わないでください。システムが正常動作しなくなります。
・本フォルダを移動する場合は、一度本システムを終了させてから移動させ、セットアップをし直してください。
・セットアップ後初回のチェックは動作していることを確認していただくために、必ずポップアップを表示させるようになっています。「今すぐ確認する」を押下ください。


【本システムで利用される技術】
・セットアップ・終了はシェルスクリプトで作られています。
・チェックは1時間に1度定期的ホームページにアクセスすることで、実現しております。
・定期実行にはlaunchctlを利用していて、セットアップを行うと下記の3つの処理がlaunchctlに登録されます。
　　・fukushi_checker : 毎時0分にホームページへアクセスし、更新があるかどうかをチェックをするために利用します。
　　・fukushi_checker_kill : 毎時5分にポップアップが表示されている場合は消去します。
　　・fukushi_checker_sleepwatcher : Macのスリープ/復帰時に本システムを一時停止/再開させるために利用します。
